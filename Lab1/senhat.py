#!/usr/bin/python
# =============================================================================
#        File : senhat.py
# Description : Displays both F and C temperatures with color changes depending
#             : on the temperature. Number of times messsage is displayed 
#             : depends on user input. Display will shut down after last message.
#      Author : John Jermain
#        Date : 1/7/2018
# =============================================================================

import sys
from sense_hat import SenseHat

print ("senhat.py [#]\n")

if len(sys.argv) > 1:
  cntMax = int(sys.argv[1])
else:
  cntMax = 2
# Create Object
sense = SenseHat()

cnt = 0
while cnt < cntMax:

    cnt += 1
    tc = sense.get_temperature()
    # fug factor 28.8  C should be like 23.5 C
    tc = .81*tc

    # C to F	Multiply by 9, then divide by 5, then add 32
    tf = ((tc*9)/5) +32

    tf = round(tf, 1)
    tc = round(tc, 1)
	
    #if tc > 18.3 and tc < 23.5:
    if tc > 18.3 and tc < 23.2:
        bg = [0, 100, 0] # green
    else:
        bg = [100, 0, 0] # red 

    msg = "Temperature = %s F or %s C" %(tf, tc)

    sense.show_message(msg, scroll_speed=0.07, back_colour=bg)
	
e = [0, 0, 0]
image = [e,e,e,e,e,e,e,e,
		e,e,e,e,e,e,e,e,
		e,e,e,e,e,e,e,e,
		e,e,e,e,e,e,e,e,
		e,e,e,e,e,e,e,e,
		e,e,e,e,e,e,e,e,
		e,e,e,e,e,e,e,e,
		e,e,e,e,e,e,e,e]
sense.set_pixels(image)
