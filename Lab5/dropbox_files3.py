#!/usr/bin/python
# =============================================================================
#        File : dropbox_files.py (modified from dropbox_sha256.py)
# Description : allows creating a password database on dropbox, list and delete files
#               in the dropbox root folder
#      Author : Drew Gislsason - author of dropbox_sha256.py
#Modifications : John Jermain & author of dropbox_files 
#        Date : 4/6/2017
# =============================================================================
# built-in libraries
import base64
import getpass
import sys
import string
import os

# installed libraries
from Crypto.Hash import SHA256
import dropbox

# See: http://md5decrypt.net/en/Sha256/
# //www.dropbox.com/developers/apps/info/appkey

TOKEN    = '3L417UmbcuAAAAAAAAAACKoZyDyqUYzkjR-rjk_mBzkksSA3MNosyBl_lI-fOE0b'
FILENAME = '/passwords.txt'
TMPFILE = 'x.txt'
SALT     = '' # '$n3@_'
INITIAL  = 'hello,486ea46224d1bb4fb680f34f7c9ad96a8f24ec88be73ea8e5a6c65260e9cb8a7\n'

DEBUG    = 0

STATUS_OK         = 0
STATUS_FAILED     = 1
STATUS_DUPLICATE  = 2

# login using secure token
def dropbox_login(token):
  dbx = dropbox.Dropbox(token)
  try:
    info = dbx.users_get_current_account()
    # print info   <- Prints user account information from dropbox
    return dbx
  except:
    print "not a valid token"
    return None
 
# get the whole file from dropbox into memory as a string
def dropbox_getfile(dbx, filename):
  try:
    # print "TMPFILE = " + TMPFILE
    os.remove(TMPFILE)        # x.txt
    # print "tmp file removed"  # When does x.txt get created?
  except:
    print "no tmp file"
	
  try:
    dbx.files_download_to_file(TMPFILE,filename)  
    # parameters: download_path to save file on local machine, path of file to download
    # print "TMPFILE = " + TMPFILE + " filename = " + filename
    # TMPFILE = x.txt filename = /passwords.txt	
    f = open(TMPFILE)  # Why is passwords.txt opened rather than TMPFILE?
    content = f.read() 
    f.close()
    # print content  <- Prints the content of the password file
    return content
  except:
    print "could not download file"
    return None
	
def dropbox_deletefile(dbx):
  while True:
    delFile = raw_input("\nEnter filename to be deleted (or type 'exit'): ")
    if delFile == 'exit':
      break
    else:  
      # Delete the file
      path1 = "/" + delFile 

      # print "path1 = " + path1

      try:
        dbx.files_delete(path1)
        # dbx.files_delete('/Mytest.txt')   <- this works as expected
        print delFile + " has been deleted"
      except Exception as e:
        print e.message, e.args
	
def dropbox_listfiles (dbx):
  print "\nFolders & Files Listing"	
  try:
    for entry in dbx.files_list_folder('',recursive=True).entries:
      print('\t name: ' + entry.name + '\t location: ' + entry.path_display)
	#root directory is iot210winterJJ 
    print '' 
  except:	
    print "could not list files"

def password_utility(dbxx,filecontentss):
  dbx = dbxx
  filecontents = filecontentss
  while True:
    # get username
    uname = raw_input("\nEnter name to login (leave empty for new user): ")
    if uname == 'exit':
      break

    # create an account
    elif uname == '':
      status, filecontents = iot_create_account(filecontents)
      if status == STATUS_OK:
        if dropbox_putfile(dbx, FILENAME, filecontents) == 0:
          print "Something went wrong"
        else:
          print "Account created"
      elif status == STATUS_DUPLICATE:
        print "Account already exists"

    # login to an account
    else:
      if iot_login_account(uname, filecontents):
        print "Welcome " + uname + "! Logged in"
      else:
        print "filecontents = \n"	+ filecontents + " uname = " + uname
        print "Login failed!!"
		
# write a dropbox file, given entire file contents.
# Returns size of file, or 0 if failed
def dropbox_putfile(dbx, filename, filecontents):
  try:
    dbx.files_delete(filename)
  except:
    print "no file to delete"
  try:
    dbx.files_alpha_upload(filecontents, filename)
    return len(filecontents)
  except:
    print "failed to upload"
    return 0

# if username matches, return password sha256
def iot_find_password_sha(uname, filecontents):

  # empty file or user, not found
  if uname == None or filecontents == None:
    return None

  # look through all the lines
  sha = None
  lines = filecontents.split('\n')
  for line in lines:
    if DEBUG: print "line " + str(line)
    i = string.find(line, ',')
    if i > 0 and uname == line[0:i]:
      sha = line[i+1:]
  return sha

# create a user with hashed password
# returns status,filecontents
def iot_create_account(filecontents):

  # don't allow creating the same name twice
  uname = raw_input("Enter username to create: ")
  if iot_find_password_sha(uname, filecontents):
    return STATUS_DUPLICATE, filecontents

  # get username and password in one line
  pwd   = SALT + getpass.getpass("Enter password: ")
  sha1  = SHA256.new(pwd).hexdigest()
  line  = uname + ',' + sha1 + '\n'

  # append it to the file
  filecontents = filecontents + line
  if DEBUG: print filecontents
  return STATUS_OK, filecontents

# login a user with hashed password
def iot_login_account(uname, filecontents):

  if DEBUG: print "uname " + str(uname)
  if DEBUG: print "file  " + str(filecontents)

  # get sha from password
  pwd   = SALT + getpass.getpass("Enter password: ")
  sha1  = SHA256.new(pwd).hexdigest()
  if DEBUG: print "sha1: " + str(sha1)

  # login if the sha matches
  sha2  = iot_find_password_sha(uname, filecontents)
  if DEBUG: print "sha2: " + str(sha2)
  if sha2 == sha1:
    return True
  return False
  
def main():
  print "dropbox_files v1.0"

  # login to dropbox
  dbx = dropbox_login(TOKEN)
  if dbx:
    print "\nlogged into dropbox"
    filecontents = dropbox_getfile(dbx, FILENAME)
    if not filecontents:
      filecontents = INITIAL
      dropbox_putfile(dbx, FILENAME, filecontents)
  else:
    print "\nfailed to log in to dropbox!"
    exit(1)
	
  while True:
    # get username
    menuChoice = raw_input("\nPlease type 1,2,3 or 4 then press Enter\n 1) Run Password Utility \n 2) List Files in Dropbox Directories \n 3) Remove a File in the Dropbox Directory \n 4) Exit\n\n  ")
    if menuChoice == '1':
      password_utility(dbx, filecontents)	
    if menuChoice == '2':
      dropbox_listfiles (dbx)
    if menuChoice == '3':
      dropbox_deletefile(dbx)
    if menuChoice == '4':
      break		  

if __name__ == "__main__":
  if len(TOKEN) <= len("{token}"):
    print "\nLooks like you didn't add your token. Go generate one for"
    print "your app API at: https://www.dropbox.com/developers/apps\n"
  else: 
    print "\nYou have a token :)"
	
  main()
